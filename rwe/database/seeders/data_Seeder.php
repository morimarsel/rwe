<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Carbon\Carbon;

class data_Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=0; $i < 20; $i++) {
            $userData[] = [
                'nama' => Str::random(10),
                'gambar' => Str::random(10).".jpg",
            ];
        }

        foreach ($userData as $user) {
            DB::table('data')->insert($user);
        }
    }
}
