@extends('layout.admin_template')

@section('title')

Insert

@endsection


@section('judul_page')

 Menu Insert

@endsection



@section('error')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
@endsection

@section('content')

  @php

  
  @endphp 
      

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
             <!--  <div class="card-header">
                
                <a href="/admin/order/insert" class="btn btn-success">INSERT</a>

              </div> -->
              <!-- /.card-header -->
              <div class="card-body">

                <div class="row">
                  <div class="col-6"><label style="font-weight: bold;">Nama:</label></div>
                  <div class="col-6"><label style="font-weight: bold;">Gambar:</label></div>
                </div>
                
                <form method="post" action="/menu/insert" enctype="multipart/form-data">
  
                  @csrf   

                  <div class="row">

                    <div class="col-6">
                        <input type="username" name="nama" style="height: 150%; width: 70%;">
                    </div>

                    <div class="col-6">
                      <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar">   
                    </div>

                  </div>

                  <br><br>                  
                  <input class="btn btn-success" type="submit" value="Tambahkan Data" required>
                </form>

                


              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection