@extends('layout.admin_template')

@section('title')

Edit

@endsection

@section('judul_page')

 Menu Edit

@endsection



@section('error')
  @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
  @endif
@endsection

@section('content')

  @php

  
  @endphp
   

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-body">
                <div class="row">
                  <div class="col-6"><label style="font-weight: bold;">Nama:</label></div>
                  <div class="col-6"><label style="font-weight: bold;">gambar:</label></div>
                </div>

                <form method="post" action="/menu/edit" enctype="multipart/form-data">

                  @csrf

                  <input type="hidden" name="id" value="{{ old('id',$id) }}">
    
                  <div class="row">

                    @foreach( $data as $row )
                    <div class="col-6">
                      <input type="username" name="nama" value="{{ old('nama',$row->nama) }}" style="height: 150%; width: 70%;">
                    </div>

                    <div class="col-6">
                      <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar">   
                    </div>

                    <input type="hidden" name="gambarlama" value="{{ old('gambarlama',$row->gambar) }}">   
                    
                    
                    @endforeach
                  </div>
                  <br><br>
                  <div class="row">
                    <div class="col-12">

                      <input class="btn btn-success mb-2" type="submit" value="Ganti Data" onclick="return confirm('Yakin ingin mengganti?');" style="width: 100%;">

                    </div>                 
                  </div>                   
                  <br>
                </form>
              </div>
              <!-- /.card-body -->
              <a href="/admin/order" class="btn btn-primary"> Kembali</a>
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection