@extends('layout.admin_template')

@section('title')

Menu

@endsection

@section('judul_page')

 Menu

@endsection


@section('error')
  
  @if (Session::has('message_update_user'))
    <div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button> 
      <strong style="color: red; z-index: 1">{{ Session::get('message_update_user') }}</strong>
    </div>
  @endif

@endsection

@section('content')

  @php

  
  @endphp


      

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                
                <a href="/menu/insert" class="btn btn-success">INSERT</a>
              </div>

              <form method="post" action="/menu" class="d-flex">

                @csrf 
                <button class="btn btn-outline-success" type="submit">Search</button>
                <input class="form-control me-2" type="search" name="search" placeholder="Search" aria-label="Search" style="text-align: center;">
              </form>

              <!-- /.card-header -->
              <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                  <thead>
                  <tr>
                    <th>No</th>
                    <th>Gambar</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                  </tr>
                  </thead>
                  <tbody>
                  @php ($i = 1)
                  @foreach( $data as $row )
                  <tr>
                    <td> {{ $i++ }} </td>
                    <td align="center"> <img src="{{asset('images/'.$row->gambar)}}" style="height: 150px; width: 150px;"></td>
                    <td> {{ $row->nama }} </td>
                    <td> <a href="/menu/edit/{{$row->id}}" class="btn btn-success"> Edit</a>
                    |
                    <a href="/menu/delete/{{$row->id}}/{{$row->gambar}}" class="btn btn-danger" onclick="return confirm('Yakin ingin menghapus?');"> Delete</a>
                    |
                    <a href="/menu/download/{{$row->gambar}}" class="btn btn-primary"> Download</a> </td>
                  </tr>
                  @endforeach

                  </tbody>
                  <tfoot>
                  <tr>
                    <th>No</th>
                    <th>Gambar</th>
                    <th>Nama</th>
                    <th>Aksi</th>
                  </tr>
                  </tfoot>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection



@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
</script>

@endsection

@section('aktif_menu')
  <?php echo "active" ?>
@endsection