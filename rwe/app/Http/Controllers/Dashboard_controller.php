<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Dashboard_controller extends Controller
{
    public function index ()
    {   
        $title ='Dasboard';

        return view('admin_view.dashboard', ['title' => $title] );             

    }
}
