<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\datum;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use File;
class Menu_controller extends Controller
{

    public function index ()
    {
        

        $data = DB::table('data')
            ->select('data.id','data.nama','data.gambar')
            ->get()

        ;


        return view('admin_view.home', ['data' => $data ] );
    }

    public function insert (Request $request){
    
        
        return view('admin_view.insert');
        

    }

    public function insert_proses (Request $request){
        
        
        
        $validated = $request->validate([

            'nama' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg'
             

        ]);


        $image = $request->file('gambar');
        $gambar_nama = time().$image->getClientOriginalName();
        $image->move(public_path('/images'),$gambar_nama);

    
        $data = new datum;
        $data->nama = $request->input('nama');
        $data->gambar = $gambar_nama;
        $data->save();


        \Session::flash('message_update_user', 'Data Telah Ditambah');

        return \Redirect::to('/menu');
        
        

    }



    public function edit (Request $request){
        
        
        $id = $request->id;

        $data = DB::table('data')
            ->select('data.nama','data.gambar')
            ->where('data.id', $id)
            ->get()
        ;

        return view('admin_view.edit', ['id' => $id, 'data' => $data ] );
        

    }


    public function edit_proses (Request $request){
        
        
        

        $validated = $request->validate([


            'nama' => 'required',
            'gambar' => 'required|mimes:jpg,png,jpeg'

        ]);

        


        $image = $request->file('gambar');
        $gambar_nama = time().$image->getClientOriginalName();
        $image->move(public_path('/images'),$gambar_nama);


        $id = $request->input('id');
        $data = datum::find($id);


        $data->nama = $request->input('nama');
        $data->gambar = $gambar_nama;
        $data->save();


        $image_lama = $request->input('gambarlama');
        $url = public_path()."/images/".$image_lama;
        File::delete($url);


        \Session::flash('message_update_user', 'Data Berhasil Diubah');

      
        return \Redirect::to('/menu');    

    }


    public function delete (Request $request){

        

        $id = $request->id;


        $data = datum::find($id);
        $data->delete();

        $gambar = $request->gambar;
        $url = public_path()."/images/".$gambar;
        File::delete($url);




        \Session::flash('message_update_user', 'Data Berhasil Dihapus');

        return \Redirect::back();
    }



    public function search (Request $request){


        $search = $request->input('search');




        $data = DB::table('data')
            ->select('data.id','data.gambar','data.nama')
            ->where('nama', 'LIKE', '%'.$search.'%')
            ->get()
        ;



        return view('admin_view.home', [ 'data' => $data ] );
    }


    public function download (Request $request){

        
        $gambar = $request->gambar;
        $url = public_path()."/images/".$gambar;
        return response()->download($url);

    }

}
