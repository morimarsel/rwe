<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Dashboard_controller;
use App\Http\Controllers\Menu_controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});




Route::get('/home',[Dashboard_controller::class, 'index']);


Route::prefix('/menu')->group(function ()  
{

    Route::get('/',[Menu_controller::class, 'index']);

    Route::post('/',[Menu_controller::class, 'search']);
    
    // insert
    Route::get('/insert',[Menu_controller::class, 'insert']);
    Route::post('/insert',[Menu_controller::class, 'insert_proses']);


    // Edit
    Route::get('/edit/{id}',[Menu_controller::class, 'edit']);
    Route::post('/edit',[Menu_controller::class, 'edit_proses']);

    // delete
    Route::get('/delete/{id}/{gambar}',[Menu_controller::class, 'delete']);

    // download
    Route::get('/download/{gambar}',[Menu_controller::class, 'download']);

});
